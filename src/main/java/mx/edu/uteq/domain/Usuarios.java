/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq.domain;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author osvaldo
 */
 @Data
 @Entity
 @Table(name = "usuarios")
 public class Usuarios {
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private long id_usuario;
    private String nombre_usuario;
    private String tipo_usuario;
    private String username;
    private String password;
    private String fecha_nac_usuario;
    private String sit_sen_usuario;
    private String estudios_anteriores_usuario;
    @OneToMany
    @JoinColumn(insertable=false, updatable=false,name = "id_usuario")
    private List<Roles> roles;
}
