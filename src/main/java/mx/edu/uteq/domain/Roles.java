/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author osvaldo
 */
@Data
@Entity
@Table(name = "roles")
public class Roles {
    @Id
   @GeneratedValue(strategy =GenerationType.IDENTITY)
    private long id_rol;
    private String nombre_rol;
    private Long id_usuario;
}
