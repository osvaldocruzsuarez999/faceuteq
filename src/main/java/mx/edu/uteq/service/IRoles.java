/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq.service;

import java.util.List;
import javax.transaction.Transactional;
import mx.edu.uteq.dao.IRolesDao;
import mx.edu.uteq.domain.Roles;

import mx.edu.uteq.domain.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author osvaldo
 */
@Service
public class IRoles implements IRolesService {

    @Autowired
    private IRolesDao RolesDao;
    

    @Override
    @Transactional
    public void registro(Roles rol) {
        RolesDao.save(rol);
    }

   
}
