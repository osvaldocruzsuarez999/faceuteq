/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.edu.uteq.service;

import java.util.List;
import javax.transaction.Transactional;
import mx.edu.uteq.dao.IUsuariosDao;
import mx.edu.uteq.domain.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author osvaldo
 */
@Service
public class IUsuarios implements IUsuariosService {

    @Autowired
    private IUsuariosDao UsuariosDao;
    
    @Override
    @Transactional
    public List<Usuarios> ListaUsuarios() {
        return (List<Usuarios>) UsuariosDao.findAll();
    }

    @Override
    @Transactional
    public void registro(Usuarios usuarios) {
        UsuariosDao.save(usuarios);
    }

   
    @Override
    @Transactional
    public Usuarios findByUsername(String username){
        return UsuariosDao.findByUsername(username);
    }

}
