/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.domain.Usuarios;

/**
 *
 * @author osvaldo
 */
public interface IUsuariosService {
    public List<Usuarios> ListaUsuarios();
    public void registro(Usuarios usuarios);//alta de usarios
   
    public Usuarios findByUsername(String username);

}
