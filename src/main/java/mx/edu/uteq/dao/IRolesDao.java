/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package mx.edu.uteq.dao;

import mx.edu.uteq.domain.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author osvaldo
 */
public interface IRolesDao extends JpaRepository <Roles,Long> {
    //Usuarios findByCorreoUsuario(String corro_usuario);
}
