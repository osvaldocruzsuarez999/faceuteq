package mx.edu.uteq.controller;

import static com.fasterxml.jackson.databind.util.ClassUtil.name;
import java.security.Principal;
import java.util.List;
import mx.edu.uteq.domain.Usuarios;
import mx.edu.uteq.service.IUsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class ControladorInicio {
@Autowired
    IUsuariosService  servicioUsuario;
    
   
     @GetMapping("/Perfil_publico")
    public String Perfil_publico(Model model) {
        return "Perfil_publico_view";
    }
      @GetMapping("/detalle_public")
    public String Detalle_public(Model model) {
        return "detalle_public_view";
    }
        @GetMapping("/muro_view")
    public String Muro(Model model,Principal principal) {
        
        principal.getName();
        if(principal.getName().isEmpty()){
        return "login";
           
        }else{
         
        model.addAttribute("dato",principal.getName());
        return "muro_view";
       
        }}
     @GetMapping("/Publicar")
    public String Publicar(Model model) {
        return "publicar";
    }
      @GetMapping("/amigos")
    public String Amigos(Model model) {
        return "amigos";
    }
    @GetMapping("/login")
    public String Login(Model model) {
        return "login";
    }
    @GetMapping("/Registro")
    public String Registro(Model model) {
        return "registro";
    }

    @GetMapping("/detalle")
    public String detalle(Model model) {
        return "detalle";
    }

    @GetMapping("/catalogo")
    public String catalogo(Model model) {
        return "catalogo";
    }

    @GetMapping("/registro")
    public String registro(Model model) {
        return "registro";
    }

    @GetMapping("/restablecer")
    public String restablecer(Model model) {
        return "restablecer";
    }

    @GetMapping("/panel")
    public String panel(Model model) {
        return "panel";
    }

    @GetMapping("/productos")
    public String productos(Model model) {
        return "productos";
    }

    @GetMapping("/usuarios")
    public String usuarios(Model model) {
        return "usuarios";
    }
    @GetMapping("/test")
    public String test(Model model,Usuarios usuarios,Principal principal) {
        //List<Usuarios> usuario ;
        principal.getName();
        //usuarios = servicioUsuario.findByUsername("prueba@gmail.com");
        usuarios = servicioUsuario.findByUsername(principal.getName());
        model.addAttribute("dato",usuarios);
        
        //model.addAttribute("datos",usuarios);
        return "test";
    }
    // controladores POST
    @PostMapping("/serviceregistro")
    public String registroUusario(Usuarios usuario){
        
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encPass = encoder.encode(usuario.getPassword());
        usuario.setPassword(encPass);
        servicioUsuario.registro(usuario);
        return "redirect:/login";
    }
    
}
