package mx.edu.uteq.security.service;

import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import mx.edu.uteq.dao.IUsuariosDao;
import mx.edu.uteq.domain.Roles;
import mx.edu.uteq.domain.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("userDetailsService")
@Slf4j
public class SecurityServiceUser implements UserDetailsService {
    
    @Autowired
    private IUsuariosDao usuariosDao;
    
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuarios usuarios = usuariosDao.findByUsername(username);
        if (usuarios==null){
            throw new UsernameNotFoundException(username);
        }
        ArrayList<GrantedAuthority> roles = new ArrayList<>();
        for(Roles rol : usuarios.getRoles()){
            roles.add(new SimpleGrantedAuthority(rol.getNombre_rol()));
        }
        User user = new User(usuarios.getUsername(), usuarios.getPassword(),roles);
        
        
        return user;
    }
}
